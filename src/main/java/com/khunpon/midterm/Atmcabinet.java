package com.khunpon.midterm;

public class Atmcabinet {
    private String name;
    private double balance;

    Atmcabinet(String name , double balance){
        this.name=name;
        this.balance = balance;
    }
    public boolean deposit(double money){
        if(money <= 0){
            return false;
        }
        balance = balance + money;
        return true;
    }
    public boolean withdraw(double money){
        if(money <= 0){
            return false;
        }
        if (money > balance){
            return false;
        }
        balance = balance - money;
        return true;
    }
    public boolean tranfermoney(double money){
        if(money <= 0){
            return false;
        }
        if (money > balance){
            return false;
        }
        balance = balance - money;
        return true;
    }
    public double getBlance(){
        return balance;
    }
    public String getName(){
        return name;
    }
    public void print(){
        System.out.println("ชื่อของคุณ : " + name + " ยอดเงินคงเหลือ : " + balance + "บาท");
    }
}
