package com.khunpon.midterm;

public class AtmcabinetApp {
    public static void main(String[] args) {
        Atmcabinet bob = new Atmcabinet("bob", 2000.0);
        bob.print();
        bob.deposit(500);
        bob.print();

        Atmcabinet song = new Atmcabinet("song", 20.0);
        song.print();
        song.withdraw(100);
        song.print();

        Atmcabinet mobile = new Atmcabinet("mobile", 1000.0);
        mobile.print();
        mobile.tranfermoney(500);
        mobile.print();
    }
}
